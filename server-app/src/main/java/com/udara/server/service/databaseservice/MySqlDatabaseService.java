package com.udara.server.service.databaseservice;

import com.udara.common.model.StudentData;
import com.udara.server.repository.StudentDataDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class MySqlDatabaseService implements DatabaseServiceInterface{
    // tables
    private static final String DETAILS_TABLE = "details";

    // columns
    private static final String INDEX_COLUMN = "_index";
    private static final String NAME_COLUMN = "_name";
    private static final String ADDRESS_COLUMN = "_address";
    private static final String AGE_COLUMN = "_age";

    private static Logger logger = LogManager.getLogger(MySqlDatabaseService.class);

    private StudentDataDao studentDataDao;

    @Override
    public void create(StudentData studentData) {
        studentDataDao.saveStudent(studentData);

    }

    @Override
    public void read() {

    }

    @Override
    public void update() {

    }

    @Override
    public void delete() {

    }
    public void setStudentDataDao(StudentDataDao studentDataDao) {
        this.studentDataDao = studentDataDao;
    }
}
