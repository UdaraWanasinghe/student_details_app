package com.udara.server.service.databaseservice;


import com.udara.common.model.StudentData;

import java.sql.SQLException;

public interface DatabaseServiceInterface {

    void create(StudentData studentData);

    void read();

    void update();

    void delete();

}
