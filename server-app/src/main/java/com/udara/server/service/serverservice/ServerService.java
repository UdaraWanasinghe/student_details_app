package com.udara.server.service.serverservice;

import com.udara.common.codec.JsonCodec;
import com.udara.common.matcher.PatternMatcherInterface;
import com.udara.common.model.Reply;
import com.udara.common.model.StudentData;
import com.udara.common.pattern.JsonPattern;
import com.udara.server.service.databaseservice.DatabaseServiceInterface;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ServerService implements ServerServiceInterface {
    private static final int SERVER_PORT = 4099;
    private static final int THREAD_POOL_SIZE = 10;

    private static Logger logger = LogManager.getLogger(ServerService.class);

    private ServerSocket mServerSocket;
    private ExecutorService mExecutorService;

    private DatabaseServiceInterface mDatabaseService;
    private PatternMatcherInterface mPatternMatcher;

    public ServerService(DatabaseServiceInterface databaseService, PatternMatcherInterface patternMatcher) throws SQLException {
        this.mDatabaseService = databaseService;
        this.mPatternMatcher = patternMatcher;
        createNewExecutorService();
    }

    @Override
    public void start() throws IOException {
        logger.debug("Starting server");
        mServerSocket = new ServerSocket(SERVER_PORT);
        logger.debug("Waiting for connection");
        while (isConnected()) {
            Socket socket = mServerSocket.accept();
            logger.debug("Connection received: " + socket.getInetAddress().toString());
            this.handleConnection(socket);
        }
    }

    @Override
    public void close() throws IOException {
        if (isConnected()) {
            mServerSocket.close();
        }
    }

    @Override
    public void handleConnection(Socket socket) throws IOException {
        RequestHandler requestHandler = new RequestHandler(socket);
        mExecutorService.submit(requestHandler);
    }

    private void createNewExecutorService() {
        stopCurrentExecutors();
        mExecutorService = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
    }

    private void stopCurrentExecutors() {
        if (mExecutorService != null && !mExecutorService.isShutdown()) {
            mExecutorService.shutdown();
        }
    }

    private boolean isConnected() {
        return mServerSocket != null && !mServerSocket.isClosed();
    }

    private class RequestHandler implements Runnable {
        private static final int TIMEOUT = 2000;
        private Logger logger = LogManager.getLogger(RequestHandler.class);

        private Socket mSocket;
        private InputStream mInputStream;
        private OutputStream mOutputStream;

        private RequestHandler(Socket socket) throws IOException {
            this.mSocket = socket;
            this.mInputStream = new BufferedInputStream(socket.getInputStream());
            this.mOutputStream = new BufferedOutputStream(socket.getOutputStream());
        }

        private void read() throws IOException {
            byte[] buffer = new byte[1024];
            int read;
            StringBuilder stringBuilder = new StringBuilder();
            long startTime = System.currentTimeMillis();
            while ((read = mInputStream.read(buffer)) != -1) {
                stringBuilder.append(new String(buffer, 0, read));
                String str = stringBuilder.toString();
                JsonPattern jsonPattern = mPatternMatcher.findPattern(str);
                if (jsonPattern.isFound()) {
                    onPatternFound(jsonPattern.getPattern());
                    break;
                } else {
                    long endTime = System.currentTimeMillis();
                    if (endTime - startTime > TIMEOUT) {
                        sendReply("Timeout triggered");
                        break;
                    }
                }
            }
            closeConnection();
        }

        void write(String str) throws IOException {
            mOutputStream.write(str.getBytes());
            mOutputStream.flush();
            logger.debug("Reply sent: " + str);
        }

        public void run() {
            try {
                read();
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        }

        private void onPatternFound(String jsonPattern) {
            StudentData studentData = JsonCodec.decodeStudentData(jsonPattern);
            try {
                mDatabaseService.create(studentData);
                sendReply("Data insertion successful");

            } catch (IOException e) {
                logger.error(e);
            }
        }

        private void sendReply(String message) throws IOException {
            Reply reply = new Reply(message);
            String encodedReply = JsonCodec.encodeReplyData(reply);
            write(encodedReply);
        }

        private void closeConnection() throws IOException {
            if (mSocket != null && !mSocket.isClosed()) {
                mSocket.close();
            }
        }
    }
}
