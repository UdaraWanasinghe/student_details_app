package com.udara.server;

import com.udara.server.listener.WrapperListener;
import com.udara.server.service.serverservice.ServerServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

import java.io.IOException;

/**
 * Server App
 */
@SpringBootApplication
@ImportResource("classpath:spring-context.xml")
@Configuration
public class ServerApplication {

    @Value("${db.driverClassName}")
    private String dbDriver;

    public static void main(String[] args) throws IOException {
        WrapperListener.listen(args);
        ApplicationContext applicationContext = SpringApplication.run(ServerApplication.class, args);
        System.out.println("Beans loaded");

        String[] str = applicationContext.getBeanDefinitionNames();
        for (String b : str) {
            System.out.println(b);
        }

        ServerApplication serverApplication = applicationContext.getBean(ServerApplication.class);
        System.out.println(serverApplication.dbDriver);

        ServerServiceInterface serverService = applicationContext.getBean(ServerServiceInterface.class);
        serverService.start();

    }
}
