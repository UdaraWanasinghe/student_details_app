package com.udara.server.repository;

import com.udara.common.model.StudentData;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;

public class StudentDataDao {
    // tables
    private static final String DETAILS_TABLE = "details";

    // columns
    private static final String INDEX_COLUMN = "_index";
    private static final String NAME_COLUMN = "_name";
    private static final String ADDRESS_COLUMN = "_address";
    private static final String AGE_COLUMN = "_age";

    private JdbcTemplate jdbcTemplate;

    private static Logger logger = LogManager.getLogger(StudentDataDao.class);

    public StudentDataDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public boolean saveStudent(StudentData studentData) {
        String query = String.format(
                "INSERT INTO %s(%s,%s,%s,%s) VALUES (?,?,?,?);",
                DETAILS_TABLE,
                INDEX_COLUMN,
                NAME_COLUMN,
                ADDRESS_COLUMN,
                AGE_COLUMN
        );

        return jdbcTemplate.update(
                query,
                studentData.getIndex(),
                studentData.getName(),
                studentData.getAddress(),
                studentData.getAge()) > 0;
    }

    /**
     * Creates tables if not exists
     *
     * @return true if table creation successful else false
     */
    private boolean prepareDatabase() {
        String query = String.format("CREATE TABLE %s(" +
                        "%s varchar(7) NOT NULL," +
                        "%s varchar(32) NOT NULL," +
                        "%s varchar(64) NOT NULL," +
                        "%s int," +
                        "PRIMARY KEY(%s));",
                DETAILS_TABLE,
                INDEX_COLUMN,
                NAME_COLUMN,
                ADDRESS_COLUMN,
                AGE_COLUMN,
                INDEX_COLUMN);

        int jdbcReturn = jdbcTemplate.update(query);
        logger.debug("Return value: " + jdbcReturn);
        return jdbcReturn == 0;
    }
}
