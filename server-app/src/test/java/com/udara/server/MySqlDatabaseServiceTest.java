package com.udara.server;

import com.udara.common.model.StudentData;
import com.udara.server.service.databaseservice.DatabaseServiceInterface;
import com.udara.server.service.databaseservice.MySqlDatabaseService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest
//@ImportResource("classpath:spring-context.xml")
@ContextConfiguration(locations = {"classpath:spring-context.xml"})
public class MySqlDatabaseServiceTest implements ApplicationContextAware {

    private ApplicationContext applicationContext;
    private DatabaseServiceInterface mySqlDatabaseService;

    @Test()
    public void createTest() {
        String[] beans = applicationContext.getBeanDefinitionNames();

        System.out.println("Beans");
        for (String b : beans) {
            System.out.println(b);
        }
        System.out.println("End of beans");

        StudentData studentData = new StudentData("164143D", "UdaraWa", "Addre", 23);
        mySqlDatabaseService.create(studentData);
    }

    public void setMySqlDatabaseService(MySqlDatabaseService mySqlDatabaseService) {
        this.mySqlDatabaseService = mySqlDatabaseService;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}