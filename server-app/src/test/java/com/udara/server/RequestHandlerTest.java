package com.udara.server;

import com.udara.server.service.databaseservice.MySqlDatabaseService;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
@Ignore
public class RequestHandlerTest {
    private ServerSocket mServerSocket;
    private Socket mSocket;
    private Thread serverThread;

    @InjectMocks
    MySqlDatabaseService mDatabaseService;

    @Before
    public void initSocketConnection() throws InterruptedException, IOException {
        startServer();
        Thread.sleep(100);
        mSocket = new Socket("127.0.0.1", 4099);
    }

    public void startServer() {
        serverThread = new Thread(() -> {
            try {
                mServerSocket = new ServerSocket(4099);
                mServerSocket.accept();
                while (true) {
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        serverThread.start();
    }

    @Test
    public void jsonRequestHandlerTest() throws IOException {

//        new Thread(jsonRequestHandler).start();
    }

    @After
    public void clearAll() throws IOException, InterruptedException {
        Thread.sleep(1000);
        mSocket.close();
        mServerSocket.close();
    }


}
