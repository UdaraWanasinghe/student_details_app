package com.udara.common;

import com.udara.common.matcher.JsonPatternMatcher;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class JsonPatternMatcherTest {
    @Mock
    JsonPatternMatcher jsonPatternMatcher;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void patternMatcherTest() {
        jsonPatternMatcher = new JsonPatternMatcher();

        String str = "{key:value}";
        Assert.assertTrue(jsonPatternMatcher.findPattern(str).isFound());

        str = "{key:value, key2:value2}";
        Assert.assertTrue(jsonPatternMatcher.findPattern(str).isFound());

        str = "{key:value, \"key2\":value2}";
        Assert.assertTrue(jsonPatternMatcher.findPattern(str).isFound());

//        String str = "{key:value} ";
//        Assert.assertTrue(jsonPatternMatcher.findPattern(str).isFound());
//
//        String str = "{key:value}";
//        Assert.assertTrue(jsonPatternMatcher.findPattern(str).isFound());
    }
}
