package com.udara.common.codec;

import com.google.gson.Gson;
import com.udara.common.model.Reply;
import com.udara.common.model.StudentData;

public class JsonCodec {

    private JsonCodec() {
    }

    public static String encodeReplyData(Reply reply) {
        Gson gson = new Gson();
        return gson.toJson(reply);
    }

    public static StudentData decodeStudentData(String data) {
        Gson gson = new Gson();
        return gson.fromJson(data, StudentData.class);
    }

    /**
     * convert StudentData object into JSON String
     *
     * @param studentData data to convert
     * @return json string
     */
    public static String encodeStudentData(StudentData studentData) {
        Gson gson = new Gson();
        return gson.toJson(studentData);
    }

    /**
     * Convert data string to StudentData object
     *
     * @param dataString data string to encode
     * @return Reply
     */
    public static Reply decodeReplyData(String dataString) {
        Gson gson = new Gson();
        return gson.fromJson(dataString, Reply.class);
    }
}
