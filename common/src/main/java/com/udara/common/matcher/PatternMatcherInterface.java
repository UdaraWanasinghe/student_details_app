package com.udara.common.matcher;

import com.udara.common.pattern.JsonPattern;

public interface PatternMatcherInterface {
    JsonPattern findPattern(String str);
}
