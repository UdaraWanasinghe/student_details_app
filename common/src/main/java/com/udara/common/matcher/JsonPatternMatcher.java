package com.udara.common.matcher;

import com.udara.common.pattern.JsonPattern;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JsonPatternMatcher implements PatternMatcherInterface {
    private Pattern mPattern = Pattern.compile("\\{(.)+}");

    @Override
    public JsonPattern findPattern(String str) {
        Matcher matcher = mPattern.matcher(str);

        if (matcher.find()) {
            return new JsonPattern(matcher.group());
        }

        return new JsonPattern(null);
    }
}
