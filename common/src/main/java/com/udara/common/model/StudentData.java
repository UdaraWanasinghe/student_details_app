package com.udara.common.model;

public class StudentData {
    public String index;
    public String name;
    public String address;
    public int age;

    public StudentData() {
    }

    public StudentData(String index, String name, String address, int age) {
        this.index = index;
        this.name = name;
        this.address = address;
        this.age = age;
    }

    @Override
    public String toString() {
        return String.format("Index: %s\nName: %s\nAddress: %s\nAge: %d", index, name, address, age);
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
