package com.udara.common.model;

public class Reply {
    private String message;

    public Reply(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }


    @Override
    public String toString() {
        return "Message: " + message;
    }
}
