package com.udara.common.pattern;

import java.io.IOException;
import java.sql.SQLException;

public interface PatternListener {
    void onPatternFound(String jsonPattern) throws SQLException;

    void onPatternError(String err) throws IOException;
}
