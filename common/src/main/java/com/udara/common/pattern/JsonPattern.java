package com.udara.common.pattern;

public class JsonPattern {
    private String mPattern;

    public JsonPattern(String mPattern) {
        this.mPattern = mPattern;
    }

    public boolean isFound(){
        return mPattern != null;
    }

    public String getPattern(){
        return mPattern;
    }
}
