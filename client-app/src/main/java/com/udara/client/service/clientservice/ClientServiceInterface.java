package com.udara.client.service.clientservice;

import java.io.IOException;

public interface ClientServiceInterface {
    public void connect() throws IOException;

    public void read() throws IOException;

    public void write(byte[] bytes) throws IOException;

    public void close() throws IOException;
}
