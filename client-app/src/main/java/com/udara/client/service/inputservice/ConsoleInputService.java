package com.udara.client.service.inputservice;

import com.udara.client.listener.InputServiceListener;
import com.udara.client.service.clientservice.ClientServiceInterface;
import com.udara.common.codec.JsonCodec;
import com.udara.common.model.StudentData;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Scanner;

public class ConsoleInputService implements InputServiceInterface, InputServiceListener<StudentData> {
    private static Logger logger = LogManager.getLogger(ConsoleInputService.class);

    private ClientServiceInterface clientService;

    public ConsoleInputService(ClientServiceInterface clientService) {
        this.clientService = clientService;
    }

    @Override
    public String[] read() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("----------------------------------");
        System.out.println("--------Input student data--------");
        System.out.println("----------------------------------");

        System.out.println("Index no: ");
        String indexNo = scanner.nextLine();
        System.out.println("----------------------------------");

        System.out.println("Name    : ");
        String name = scanner.nextLine();
        System.out.println("----------------------------------");

        System.out.println("Address : ");
        String address = scanner.nextLine();
        System.out.println("----------------------------------");

        System.out.println("Age     : ");
        String age = scanner.nextLine();
        System.out.println("----------------------------------");

        return new String[]{
                indexNo,
                name,
                address,
                age
        };
    }

    public void prompt() {
        try {
            clientService.connect();

            String[] data = read();
            String index = data[0];
            String name = data[1];
            String address = data[2];
            int age = Integer.parseInt(data[3]);
            StudentData studentData = new StudentData(index, name, address, age);

            if (isValid(studentData)) {
                this.onInputSuccess(studentData);
            } else {
                logger.error("Invalid inputs");
                prompt();
            }
        } catch (IOException e) {
            logger.error(e);
        }
    }

    public boolean isValid(StudentData studentData) {
        boolean isValid = true;
        if (studentData.getIndex().length() != 7) {
            isValid = false;
            logger.error("Index no is invalid: " + studentData.index);
        }
        if (studentData.getName().length() < 6) {
            isValid = false;
            logger.error("Username must be more than 6 characters in length: " + studentData.name);
        }
        if (studentData.getAddress().isEmpty()) {
            isValid = false;
            logger.error("Address can't be empty: " + studentData.address);
        }
        if (studentData.getAge() < 12) {
            isValid = false;
            logger.error("Age should be more than 12: " + studentData.name);
        }
        return isValid;
    }

    @Override
    public void onInputSuccess(StudentData studentData) {
        String str = JsonCodec.encodeStudentData(studentData);
        try {
            clientService.write(str.getBytes());
            clientService.read();
            clientService.close();
        } catch (IOException e) {
            logger.error(e);
        }
    }
}
