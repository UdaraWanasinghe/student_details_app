package com.udara.client.service.clientservice;

import com.udara.client.listener.ClientServiceListener;
import com.udara.common.codec.JsonCodec;
import com.udara.common.matcher.PatternMatcherInterface;
import com.udara.common.model.Reply;
import com.udara.common.pattern.JsonPattern;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.Socket;

public class DatabaseClientService implements ClientServiceInterface, ClientServiceListener {
    private static final String SERVER_IP = "127.0.0.1";
    private static final int SERVER_PORT = 4099;
    private static final int REPLY_TIMEOUT = 2000;

    private static Logger logger = LogManager.getLogger(DatabaseClientService.class);

    private Socket socket;
    private OutputStream mOutputStream;
    private InputStream mInputStream;

    private PatternMatcherInterface mPatternMatcher;

    public DatabaseClientService(PatternMatcherInterface patternMatcher) {
        this.mPatternMatcher = patternMatcher;
    }

    @Override
    public void connect() throws IOException {
        if (isConnected()) {
            logger.debug("Client is already connected to: " + socket.getInetAddress().toString());
        } else {
            logger.debug("Connecting to server");
            socket = new Socket(SERVER_IP, SERVER_PORT);
            logger.debug("Server connection success");
            mInputStream = new BufferedInputStream(socket.getInputStream());
            mOutputStream = new BufferedOutputStream(socket.getOutputStream());
        }
    }

    @Override
    public void read() throws IOException {
        byte[] buffer = new byte[1024];
        StringBuilder stringBuilder = new StringBuilder();
        int read;
        long startTime = System.currentTimeMillis();
        while ((read = mInputStream.read(buffer)) > 0) {
            stringBuilder.append(new String(buffer, 0, read));
            String str = stringBuilder.toString();
            JsonPattern jsonPattern = mPatternMatcher.findPattern(str);
            if (jsonPattern.isFound()) {
                this.onDataReceive(jsonPattern.getPattern());
            }
            long endTime = System.currentTimeMillis();
            if (endTime - startTime > REPLY_TIMEOUT) {
                this.onConnectionTimeout();
                this.close();
                break;

            }
        }
    }

    @Override
    public void write(byte[] bytes) throws IOException {
        if (isConnected()) {
            mOutputStream.write(bytes);
            mOutputStream.flush();
        } else {
            logger.debug("Not connected to the server");
        }
    }

    @Override
    public void close() throws IOException {
        if (isConnected()) {
            socket.close();
        } else {
            logger.debug("Connection is already closed");
        }
        socket = null;
    }

    /**
     * check whether there is an active connection
     *
     * @return true if there is an ongoing connection, else false
     */
    private boolean isConnected() {
        return socket != null && socket.isConnected() && !socket.isClosed();
    }

    @Override
    public void onConnectionSuccess() {

    }

    @Override
    public void onDataReceive(String str) {
        Reply reply = JsonCodec.decodeReplyData(str);
        logger.info(reply.getMessage());
    }

    @Override
    public void onConnectionTimeout() {
        logger.debug("Connection timeout triggered");
    }
}
