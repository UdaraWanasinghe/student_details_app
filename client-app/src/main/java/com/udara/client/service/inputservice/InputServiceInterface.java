package com.udara.client.service.inputservice;

public interface InputServiceInterface {
    public String[] read();
}
