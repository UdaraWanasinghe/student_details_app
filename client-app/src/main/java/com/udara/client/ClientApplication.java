package com.udara.client;

import com.udara.client.listener.WrapperListener;
import com.udara.client.service.inputservice.ConsoleInputService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ImportResource;

/**
 * Client Application
 */
@SpringBootApplication
@ImportResource("spring-context.xml")
public class ClientApplication {
    public static void main(String[] args) {
        WrapperListener.listen(args);
        ApplicationContext applicationContext = SpringApplication.run(ClientApplication.class, args);
        ConsoleInputService inputService = applicationContext.getBean(ConsoleInputService.class);
        inputService.prompt();
    }
}