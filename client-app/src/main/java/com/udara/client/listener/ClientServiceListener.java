package com.udara.client.listener;

import java.io.IOException;

public interface ClientServiceListener {
    public void onConnectionSuccess() throws IOException;

    public void onDataReceive(String str);

    public void onConnectionTimeout();
}
