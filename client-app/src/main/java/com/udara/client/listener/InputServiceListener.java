package com.udara.client.listener;

import com.udara.common.model.StudentData;

public interface InputServiceListener<T> {
    void onInputSuccess(T t);
}
