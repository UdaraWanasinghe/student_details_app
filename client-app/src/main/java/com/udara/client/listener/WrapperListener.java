package com.udara.client.listener;

import org.tanukisoftware.wrapper.WrapperManager;

public class WrapperListener extends Thread implements org.tanukisoftware.wrapper.WrapperListener {
    private String[] mArgs;

    public WrapperListener(String[] args) {
        this.mArgs = args;
    }

    @Override
    public Integer start(String[] strings) {
        return null;
    }

    @Override
    public int stop(int i) {
        return 0;
    }

    @Override
    public void controlEvent(int i) {

    }

    @Override
    public void run() {
        WrapperManager.start(this, mArgs);
    }

    public static void listen(String[] args) {
        new WrapperListener(args).start();
    }
}
