package com.udara.client;

import com.udara.client.service.inputservice.ConsoleInputService;
import com.udara.common.model.StudentData;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class InputServiceTest {

    @Test
    public void studentDataValidationTest() {
        ConsoleInputService consoleInputService = new ConsoleInputService(null);
        StudentData studentData = new StudentData("164143D", "UdaraWa", "Address", 23);
        Assert.assertTrue(consoleInputService.isValid(studentData));
    }

}
